package de.tarent.maven.plugins.buildID;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.maven.model.Build;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3Dom;

/**
 * @goal BuildID
 */
public class BuildIDMojo extends AbstractMojo
{	
	/**
	 * The Project
	 * 
	 * @parameter expression="${project}"
	 */
	private MavenProject project; 
	
    public void execute() throws MojoExecutionException{
    	Log log = getLog();
    	Plugin plugin = null;
    	StringBuffer key = genBuildId();
    	Build build = project.getBuild();
    	
    	try{
	    	List<Plugin> list = build.getPlugins();
	    	Iterator<Plugin> itPlugin = list.iterator();
	    	while(itPlugin.hasNext()){
	    		plugin = itPlugin.next();
	    		if(plugin.getArtifactId().equalsIgnoreCase("maven-jar-plugin")
	    				|| plugin.getArtifactId().equalsIgnoreCase("maven-war-plugin")){
	    			Xpp3Dom config = (Xpp3Dom)(plugin.getConfiguration());
	    			Xpp3Dom temp = new Xpp3Dom("Build-ID");
	    			temp.setValue(key.toString());
	    			config.getChild("archive").getChild("manifestEntries").addChild(temp);
	    			plugin.setConfiguration(config);
	    		}
	    	}
    	}catch(NullPointerException e){
    		// Catch NullPointerException, if Manifest-Entries aren't configured
    		log.warn("generateKey-Plugin (wrong Configuration)");
    	}
        log.info("Generated Key: " + key);
    }
    
    private StringBuffer genBuildId(){
    	StringBuffer key = new StringBuffer();
    	Date date = new Date();
    	String user = System.getProperty("user.name");
       	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss:SS");
       	    	
    	key.append(user);
    	key.append("_");
    	key.append(getHost());
    	key.append("_");
    	key.append(df.format(date));
    	
    	return key;
    }
    
    public String getHost(){
    	String hostname = "";
		try {
	        InetAddress addr = InetAddress.getLocalHost();
	        hostname = addr.getHostName();
	    } catch (UnknownHostException e) {
	    	e.printStackTrace();
	    }
		return hostname;
    }
}